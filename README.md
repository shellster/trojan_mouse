# Trojan Mouse Village

This document was prepared for BsidesBoise2019.  

This project is very much a "do-it-yourself" project.  It requires significant soldiering skills.  Please discuss your project with the Village Staff to make sure you know what you are doing as there are no spare parts if you irreparably damage your hardware.

## Overview

The brains of your trojan mouse is the Wifi HID Injector:
https://github.com/whid-injector/WHID

Before proceeding you may wish to make sure your device is running the latest firmware by following the directions here: https://github.com/exploitagency/ESPloitV2

At the bottom of this page is a pinout for the USB holes on the Wifi HID Injector.  It can be removed from the plastic casing with a bit of care.

To allow the Mouse to continue to function, we will be using one of these mini USB hubs: https://www.tindie.com/products/mux/nanohub-tiny-usb-hub-for-hacking-projects/

The project follows these steps:

**DO NOT PROCEED WITHOUT TALKING TO VILLAGE STAFF**

1) Carefully disassemble the mouse.
2) Cut the USB cable to the mouse, inside the mouse body.  You will want to exercise caution when you do this to make sure you cut the cable in such a way that the nano hub will fall in a good place inside the mouse body.
3) Hook the mouse cable to the correct holes on the NanoUSB.  These are the four holes that are on one side by themselves.
4) Hook the wires on the old mouse plug to one of the NanoUSB hub's slave ports.
5) Hook the ribbon cable ot the Wifi HID Injector.  There is significant technique to how to clean the ribbon wire ends for soldering (see Village Staff)
6) Hook the other end of the ribbon cable to the other NanoUSB hub slave port.
7) Hot glue everything into place in the mouse body.
8) Reassemble the mouse.
9) Profit

**Failure to correctly solder the right wires to the right locations could result in ruined hardware, including whatever you plug the USB cable into.  Make sure you carefully solder up your wires and validate them with a continuity connector.**

## Pinout

According to two people doing this project, the mouse cable colors correspond to the USB pinout as follows:

| Number |   USB  | Color  |
| ------ | ------ |--------|
|   1    |   VCC  | White  |
|   2    |   D-   | Green  |
|   3    |   D+   | Blue   |
|   4    |   GND  | Orange |


Here is the pinout of the WHID Injector

![](Images/whid_injector.jpg)

Here's the pinout of the USB plug:

![](Images/usb_pinout.png)